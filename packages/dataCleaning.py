import pandas as pd
import numpy as np


def cleanData(securities, cleaningParams, fileNameIn, fileNameOut, filePath):
    ''' Wrapper for high frequency data cleaning; results are written as .csv files
    Args:
        securities = list of (user defined) type 'security'
        cleaningParams = dict of with keys {'missingReturnThreshold', 'zeroReturnThreshold', 'doDiagnostics'} for various data cleaning parameters / filters
        filePath = dict with keys {'in', 'out'} for input and output file paths, respecively
        fileNameIn = list of strings containing names of files in 'filePath['in']'
        fileNameOut = dict with keys {'intraday', 'overnight'} specifying beginning of output file names
    Output:
        void (data is written to file)
    '''
    
    # For each security...
    for idx, s in enumerate(securities):
      
        # Import csv data 
        data = pd.read_csv(filepath_or_buffer = filePath['in'] + '\\' + fileNameIn[idx], sep = ',')
        
        # Check each data file contains only one security (as identified by the RIC)
        securityName = data['#RIC'].unique().tolist()
        if len(securityName) != 1:
            raise ValueError('Multiple securities in single file.')
        if securityName[0] != s.name:
            raise ValueError('Securities object specification misordered)')
            
        # Check return peiod is correct
        if '15' not in data['Type'][0]:
            raise ValueError('Check return period ("retPeriod") specified matches data.')
        
        # See if any corrections exist in the data    
        corrections = data[~ np.isnan(data['Correction Qualifiers'])] 
        if corrections.size != 0:
            raise ValueError('Corrections exist in the data need to implement handling of this...')
        
        # If we have input 'open' price data convert this data column NAME to 'last' so that we do not need to adjust code in the sequel
        if 'Open' in list(data.columns.values):
            data.rename(columns = {'Open': 'Last'}, inplace = True)
            print("Warning:'Open' price data variable renamed 'last' in output. Open price data has been used but we are too lazy to change every instance of 'last' that appears in our code.")
        
        # Rename variables
        data = data.rename(columns = {'#RIC': 'RIC', 'Date[L]': 'date', 'Time[L]': 'time', 'Last': 'last'})                  
                                      
        # Remove observations without price &/or timestamp
        data = data.dropna(subset = ['last', 'time'])
        
        # Convert time and date to datetime object (slow)
        data['datetime'] = pd.to_datetime(data['date'] + ' ' + data['time'])
        
        # Create separate time and date variables from 'datetime' (rather than converting existing time & date from strings)
        data = data.drop(columns = ['date', 'time'])
        data['date'] = [x.date() for x in data['datetime']]
        data['time'] = [x.time() for x in data['datetime']]
        
        # Remove unwanted variables 
        data = data.drop(columns = ['RIC', 'Type', 'No. Trades', 'Correction Qualifiers'])
    
        # Days in dataset
        date = data['date'].unique().tolist()
    
        # Returns preallocation
        intraDayRet = pd.DataFrame(columns = ['datetime', 'logRet'])
        overnightRet = pd.DataFrame(columns = ['date', 'logRet'])
        
        # Diagnositcs preallocation
        if cleaningParams['doDiagnostics']:
            missingDataDates = pd.DataFrame(columns = ['date', 'numMissingRet'])
            staleDataDates = pd.DataFrame(columns = ['date', 'numStaleRet'])
          
        # For each day...
        for d in date:
           
            # Get data
            dailyData = data[data['date'] == d]
           
            # Remove prices outside pre-specified market hours
            dailyData = dailyData[(dailyData['time'] >= s.marketOpen) & 
                                  (dailyData['time'] <= s.marketClose)]
    
            # Variables for testing daily data suitability
            numDailyReturns = max(len(dailyData) - 1, 0)       # nb: (i) maximum to for case when day has zero observations; (ii) loose one data point in passing to returns
            numMissingReturns = s.numReturnsPerDay - numDailyReturns
            numZeroReturns = sum(dailyData['last'].diff()[1:] == 0.0)
            
            # Missing data diagnostics (save relevant info if there are issues with daily data)
            if cleaningParams['doDiagnostics']:
                # If intraday returns are missing 
                if numDailyReturns != s.numReturnsPerDay:       
                    missingDataDates = missingDataDates.append(pd.DataFrame(data = [[d, numMissingReturns]], columns = ['date', 'numMissingRet']))
                # If intraday returns are exactly zero (potentially stale price data)
                if numZeroReturns != 0.0:
                    staleDataDates = staleDataDates.append(pd.DataFrame(data = [[d, numZeroReturns]], columns = ['date', 'numStaleRet']))
        
            #  Skip day if returns are missing or prices do not change (stale quotes) 
            if numMissingReturns > cleaningParams['missingReturnThreshold']: continue    # Intraday prices missing data is missing 
            elif numZeroReturns > cleaningParams['zeroReturnThreshold']: continue        # Intraday prices do not change
                      
            # Get daily intraday log returns (nb: .diff() is a forwards difference, i.e. array[i+1] - array[i], & time is in ascending order)
            dailyData['logRet'] = np.log(dailyData['last']).diff()
      
            # Concatenate intraday returns
            intraDayRet = intraDayRet.append(dailyData[['datetime', 'logRet']][1:])
                    
            # Get overnight retuns (using yesterday's close price, which is computed later in the for loop for 'd')
            if 'closePrice' in locals(): # skip return computation on 1st day when we only have a single price (can't use 'if d != date[0]' b/c 1st day of data might be skipped due to missing observations etc  )
                overnightRet = overnightRet.append(pd.DataFrame([[d,  np.log(dailyData['last'].iloc[0] / closePrice)]], 
                                                                  columns = ['date', 'logRet']))
                # if 'numMissingReturns' or 'numZeroReturns' sufficiently high then day is skipped and no return is computed and no close price is obtained for computing next day's return
           
            ''' FIX: O/NIGHT RETURNS SHOULD BE COMPUTED WHEN EVEN WHEN THERE ARE ONLY A SMALL NUMBER OF INTRADAY RETURNS '''
            
            
            ''' any more calculations before final computation of computing close price ... '''
            
            
            # Get last close price for current day (this must go at end of daily loop as we use yesterday's close for today's overnight return)
            closePrice = dailyData['last'].iloc[0]
            
        # Get date range for security data (returns, not prices) after cleaning (can use overnight returns since dates should be same for intraday returns)
        s.getDataDateRange(overnightRet['date'].values[0], overnightRet['date'].values[-1])
    
        # Output file names   
        fileNameOutTemp = fileNameOut.copy()   # reset base of file name for each security
        fileNameOutTemp['intraday'] += "_" + str(s.returnFrequency) + "m_"+ s.name[1:] + "_" + s.dataStart.strftime('%d%b%Y') + "to" + s.dataEnd.strftime('%d%b%Y') + '.csv'
        fileNameOutTemp['overnight'] += "_" + s.name[1:] + "_" + s.dataStart.strftime('%d%b%Y') + "to" + s.dataEnd.strftime('%d%b%Y') + '.csv'  
        
        # Prepend file paths for output data
        pathOut = {k: filePath['out']  + "\\" + name  for k, name in fileNameOutTemp.items()}   
           
        # Write to output file
        for idx2, r in enumerate([intraDayRet, overnightRet]):
            r.to_csv(path_or_buf = pathOut[list(pathOut.keys())[idx2]], encoding = 'utf-8', index = False)        
        
        # Write diagnostic data to file ('missingDataDates' and 'staleDataDates' dataframes)
        if cleaningParams['doDiagnostics']:
            print('printing of diagnostic results is not implemented...')
            ''' implementation here '''
