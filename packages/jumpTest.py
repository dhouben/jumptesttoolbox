''' Package for jump tests of realised financial log returns '''

''' Module imports '''
import pandas as pd
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
from scipy.stats import skew, kurtosis, norm, probplot, gaussian_kde, mstats
from scipy import special
import pickle                # for saving python variables to file


''' Global variables '''
c_GLOBAL  = np.sqrt(2.0 / np.pi)             # used in Lee & Mykland and ABD jump test
c2_GLOBAL = np.pi / 2.0                      # used in ABD jump test (equivalent to pow(c_GLOBAL, -2))
c3_GLOBAL = np.pi * np.pi / 4.0 + np.pi - 5  # used in BNS jump test (ratio version, cf. Huang & Tauchen (2005); see Dumitru & Urga (Eq, 5, 2012) for an approximate numerical version (approx. 0.61))
c4_GLOBAL = pow(pow(2, 2.0 / 3.0) * special.gamma(7.0 / 6.0) / np.sqrt(np.pi), - 3.0) # used in BNS jump test (cf. Huang & Tauchen (p. 461, 2005) and Dumitru & Urga (Eq, 6, 2012) for a numerical value)

''' User defined functions ''' 

def realisedVariance(data):
    ''' Compute realised variance from a set of returns data (sum of squared log returns). '''    
    return sum([r * r for r in data])

def bipowerVariation(data):
    ''' Compute bipower variation from a set of returns data. Method is from Huang & Tauchen (p. 459) 
    and includes an adjustment factor which is not present in Barndorf Nielsen (2006), but dissapears 
    asymptotically. '''
    return (len(data) / (len(data) - 1)) * sum(np.multiply(abs(data)[1:], abs(data)[0:-1])) * c2_GLOBAL

def tripowerVariation(data):
    ''' Compute tripower variation from a set of returns data. Method is from Huang & Tauchen (p. 461) 
    and includes numerical value of constant c4_GLOBAL (approx. 1.74) can be found in Dumitru & Urga
    (Eq, 6, 2012). '''
    M = len(data)
    return c4_GLOBAL * M  * (M / (M - 2)) * sum(pow(abs(np.multiply(np.multiply(data[0:-2], data[1:-1]), data[2:])), 4.0 / 3.0))
    
''' Classes representing various jump tests '''

#class ABDtestStatistic:
#    ''' Class for test statistic of Andersen, Bollerslev, and Diebold (2007), Journal of Econometrics. ''' 
#    
#    def __init__(self, numObsPerDay = None, returnsData = None):
#        # Set additional optional attributes
#        if (returnsData is not None) and (type(returnsData) == type(pd.DataFrame())):
#            self.returnsData = returnsData
#    
#    def computeBipowerVariation(self, adjustmentFactor = False):
#        ''' Compute realized bipower variation based on single trading day data, cf. Eq. (12) of ABD (2007)
#        Nb:
#            - adjustmentFactor scales BV cf. Clements & Liao (Eq. 4, 2017) International Journal of Forecasting
#            - Differenes to LM test BV: 
#                (i) LM uses window which may span multiple days, ABD only a single day, and all daily returns are used; 
#                (ii) LM excludes return to be tested from BV, ABD does not
#        '''
#        temp = abs(self.returnsData['logRet'])      # get absolute value of ALL log returns in given day
#        if adjustmentFactor:
#            numObsPerDay = len(temp)                # number of intraday returns in the day
#            return((numObsPerDay / numObsPerDay - 1) * sum(np.multiply(temp[1:], temp[0:-1])) / c2_GLOBAL)
#        else:
#            return(sum(np.multiply(temp[1:], temp[0:-1])) / c2_GLOBAL)

    
class LMtestStatistic:
    ''' Class for test statistic of Lee & Mykland (2008) RFS - Jumps in financial markets: a new nonparametric test and jump dynamics. ''' 
    def __init__(self, numObsPerDay = None, returnsData = None, windowSize = None):
        ''' Constructor 
        SHOULD NOT COMPUTE WINDOW SIZE EACH TIME OBJECT IS CREATED (SLOWS THISNGS DOWN) ''' 
        # Set number of return observations per day
        if numObsPerDay is not None:
            self.numObsPerDay = numObsPerDay       # will come from security object, in general
                
        # Set window size on initialisation
        if (windowSize is not None) and (type(windowSize) == int):
            self.windowSize = windowSize # if passed manually on initialisation
        else:
            if (numObsPerDay is None):
                raise ValueError('To compute window size, number of observations per day must be specified.')
            self.getWindowSize()         # if not passed on initialisation compute 
        
        # Set additional optional attributes
        if (returnsData is not None) and (type(returnsData) == type(pd.DataFrame())):
            # Check returns input data is correct (K - 1 = windowSize - 1 returns for estimating volatiltiy PLUS one (1) return for jump test; i.e. K returns in total)
            if self.windowSize != returnsData.shape[0] :
                raise ValueError('Data window should have same number of rows as default window size (%d)' % self.windowSize)
            self.returnsData = returnsData
    
    def getWindowSize(self):
        ''' Get recommended window size for window based on number of observations per day, cf. Lee & Mykland (p. 2542, 2008) 
        (Nb: we do not used fixed recommended sizes in Lee & Mykland as UK & US securities have different number of observations
        per day for the same return frequency.)''' 
        self.windowSize = np.ceil(np.sqrt(252 * self.numObsPerDay))
    
    def computeBipowerVariation(self):
        ''' Compute realized bipower variation for window, cf. Eq. (8) of Lee & Mykland (2008)
        (Nb: uses all log returns in window excluding most recent) '''
        temp = abs(self.returnsData['logRet'][:-1])
        return(sum(np.multiply(temp[1:], temp[0:-1])) / (len(temp) - 1)) # nb: len(output) == (self.windowSize - 2) should be True
#        # In detail:
#        temp = abs(self.returnsData['logRet'][:-1])  # get absolute value of log returns in window excluding most recent
#        temp2 = np.multiply(temp[1:], temp[0:-1])    # compute terms in bipower variation (lag by one and multiply element-wise)
#        return(sum(temp2) / len(temp2))              # return sum of terms normalised by number of terms (nb: len(temp2) == (self.windowSize - 2) should be True)
             
    def computeTestStatistic(self):
        ''' Eq. (7) of Lee & Mykland (2008); test stat = most recent log return in window divided 
        by squrae root of realized bipower variation '''
        return(self.returnsData['logRet'].iloc[-1] / np.sqrt(self.computeBipowerVariation()))
       
    def Cn(self, numObs):
        ''' Location factor for test statistic (cf. Lemma 1 of Lee & Mykland (2008)).
        Nb: numObs = number of test statistics included in maximum statistic. Commonly taken as numObsPerDay
        when the number of observations per day is large, ex. lements & Liao (2017) ''' 
        ##### THERE IS A TYPO SOMEWHERE IN LEE & MYKLAND (EQ. 13 2008) [CF. Gilder et al (2014) JBF Co-jumps in stock prices, footnote 6] #########
        return np.sqrt(2 * np.log(numObs)) / c_GLOBAL - (np.log(4 * np.pi) + np.log(np.log(numObs))) / (2 * c_GLOBAL * np.sqrt(2 * np.log(numObs)))
    
    def Sn(self, numObs):
         ''' Scale factor for test statistic (cf. Lemma 1 of Lee & Mykland (2008)) ''' 
         return 1 / (c_GLOBAL * np.sqrt(2 * np.log(numObs)))
       
    def computeThreshold(self, alpha, numObs):
        ''' Threshold for test statistic based on a user specified confidence level (1 - alpha) and the 
        asymptotic distribution in Lee & Mykland (Lemma 1, 2008). See also Clements & Liao (Eq. 14, 2017) '''
        return (- np.log( - np.log(1 - alpha))) * self.Sn(numObs) + self.Cn(numObs)
    
    def hypothesisTest(self, alpha, numObs):
        ''' Hypothesis test for jump,  cf. Lee & Mykland (p. 2543, 2008). If test absolute value of 
        the statistic is larger than threshold we reject the null hypothesis of no jump. See also,
         Clements & Liao (Eq. 14, 2017)'''
        return (abs(self.computeTestStatistic()) > self.computeThreshold(alpha, numObs))
        
    def info(self):
        ''' Print selected attributes of test statistic '''
        if hasattr(self, 'numObsPerDay'): print(self.numObsPerDay)
        if hasattr(self, 'windowSize'): print(self.windowSize)
   
        
def jumpTest(security, testType, testParams, fileNameClean, filePath, variableSaveFolder, variableSaveName):
    ''' Script for implementation of Lee & Mykland (2008), Andersen et al (2007) and BNS (2006) jump tests
    Args:
        - security: security object as defiend in security.py package
        - testType: {LM, ABD, BNS}
        - testParams: dict of parameters for relevant testType (see main.py for specification ex.)
        - fileNameClean: dict with keys {'intraday', 'overnight'} and values containing list of strings  
        with respecitve file names (see main.py for specification ex.)
        - filePath: dict with keys {'in', 'out'} and values containing strings of input and output 
        file paths for high frequency returns data (we only care about 'out' == cleaned data)
        - variableSaveFolder: string with folder (incl. path) for saving jump test results as variable (cf. main.py; there should
        be a file name for each cleaned HF dataset)
        - variableSaveName: string with desired save variable name (cf. main.py)
    Nb:
        - file called in main.py
        - output is dependent on test called , but in all cases a data frame is returned
    ''' 
   
    # For given security, get position in fileNameClean['intraday']      
    idxNameClean = [fileNameClean['intraday'].index(l) for l in fileNameClean['intraday'] if security.name[1:] in l][0]

    # Read in intraday data as pandas DataFrame
    data = pd.read_csv(filepath_or_buffer = filePath['out'] + '\\' + fileNameClean['intraday'][idxNameClean], sep = ',')
   
    ''' #### REMOVE ANY EXTREME RETURNS: LOOK INTO WHY THESE EXIST ####'''    
    removalThreshold = 0.2;
    data = data[data['logRet'].between(- removalThreshold, removalThreshold, inclusive = False)]
    print("Intraday returns larger than %.0f percent are removed as outliers. THESE SHOULD BE LOOKED INTO..." % (removalThreshold * 100.0))

    # Recast date & time string to datetime.datetime object (nb: this will appear as a Timestamp object, cf.: https://stackoverflow.com/questions/23755146/why-does-pandas-return-timestamps-instead-of-datetime-objects-when-calling-pd-to)
    data['datetime'] = pd.to_datetime(data['datetime'], format = '%Y-%m-%d %H:%M:%S')   # Nb.: use .to_pydatetime() to recover datetime.datetime object from pandas Timestamp
    
    # Get unique dates in dataset (used in BNS and ABD tests)
    dates = uniqueOrderedList([ll.to_pydatetime().date() for ll in data['datetime']]) # for unordered list:  list(set([ll.to_pydatetime().date() for ll in data['datetime']]))
    
    # Set test spacific parameters
    if testType == "LM":
        # Set the optimal window size
        testParams['windowSize'] = int(np.ceil(np.sqrt(252 * security.numReturnsPerDay)))   # (security dependent) window size based on recommendation in Lee & Mykland (p. 2542, 2008)
        
        # Get the number of test statistics that can be generated
        testParams['n'] = data.shape[0] - testParams['windowSize'] + 1                   # number of test statistics than can be generated given the length of the data set and window, respectively. Note that we add one (+1) to account for the fact that the last observation in the window is tested for the jump.               
        print('NEED TO CHECK THAT "n" HERE IS THE SAME AS THAT IN LEE & MYKLAND (2008)')
        
        # Initialise list for saving jump test output
        jumpDataCols = ['datetime', 'logRet', 'vol', 'testStat']
        jumpTestData = [[] for i in range(len(jumpDataCols))]    
        
        # For each possible jump test time (all intraday periods less reduction due to window)
        for idx_n, n in enumerate(range(testParams['n'])):
            # Select data for window (where most recent return is the jump test epoch)
            windowData = data[idx_n:(testParams['windowSize'] + idx_n)]
            
            # Initialise object for obtaining test statistic for jump return
            L = LMtestStatistic(windowSize = testParams['windowSize'], returnsData = windowData)
            
            # Get output (we get intermediary values such as vol as this time series is of interest in and of itself)
            # Nb.:
            #   - We do not get threshold value for test statistic. This depends on: (i) numObs (which could be interpreted as 
            #   number of intraday return observations); and (ii) alpha. Since our data is cleaned (approx. same number of
            #   intraday returns each day), and if we take numObs = numObsPerDay, we can compute the threhold value after the
            #   fact, as its value wont change through time.
            datetime = windowData['datetime'].iloc[-1]
            logRet = windowData['logRet'].iloc[-1]
            vol = np.sqrt(L.computeBipowerVariation())
            testStat = L.computeTestStatistic()
            
            # Append output to list
            for idx_m, metric in enumerate([datetime, logRet, vol, testStat]): # should match jumpDataCols
                jumpTestData[idx_m].append(metric)
        
        # Convert jump test data into pandas dataframe (lists used in loop for speed)     
        jumpTestData = pd.DataFrame(jumpTestData)
        jumpTestData = jumpTestData.transpose()
        jumpTestData.columns = jumpDataCols
        
        # Variable save subfolder
        subFolder = 'LMtest'
                
    elif testType == "ABD":
        # Initialise list for saving jump test output      
        #vol = []  # for time series of daily vol
        paddedVol = []
        testStat = []
        
        for idx_d, date in enumerate(dates):
            # Get all data for given trading day (noting that data is formatted by timestamp)
            dateMask = (data['datetime'] > date) & (data['datetime'] < (date + dt.timedelta(days = 1)))
            dailyData = data.loc[dateMask]
            
            # Number of returns per day (should be same as security.numReturnsPerDay for most days, cf. filters for skipped days in dataset)
            M = dailyData.shape[0]
            
            # Compute (& concatenate) square root of scaled bipower variation (similar to vol estimate in LM test); nb. vol is per day (not per intraday return as in LM)
            volTemp = np.sqrt(bipowerVariation(dailyData['logRet']) / M)
            #vol.append(volTemp)
            
            # Pad INTRADAY output with DAILY vol estimate (creates duplicate info, but makes plotting etc easier later)
            paddedVol += [volTemp] * M
            
            # Compute (& concatenate) intraday test statistics            
            testStat += list(np.divide(dailyData['logRet'], volTemp))
        
        # Convert jump test data into pandas dataframe
        jumpDataCols = ['datetime', 'logRet', 'vol', 'testStat']
        jumpTestData = pd.DataFrame([list(data['datetime']), list(data['logRet']), paddedVol, testStat]).transpose()
        jumpTestData.columns = jumpDataCols
           
        # Variable save subfolder
        subFolder = 'ABDtest'
        
        # print("ABD not implemented")
    elif testType == "BNS":
        
        # Initialise list for saving jump test output
        jumpDataCols = ['date', 'RV', 'BV', 'testStat', 'logRetMax', 'timeMax']
        jumpTestData = [[] for i in range(len(jumpDataCols))]  
        
        for idx_d, date in enumerate(dates):
            # Get all data for given trading day (noting that data is formatted by timestamp)
            dateMask = (data['datetime'] > date) & (data['datetime'] < (date + dt.timedelta(days = 1)))
            dailyData = data.loc[dateMask]
            
            # Number of returns per day (should be same as security.numReturnsPerDay for most days, cf. filters for skipped days in dataset)
            M = dailyData.shape[0]
            
            # Get largest (by magnitude) intraday return (incl. sign of return)                 
            # Nb: need to index w/ .loc not .iloc since .idxmax and .idxmin return LABELS from index not POSITIONS from index (cf. https://stackoverflow.com/questions/31593201/pandas-iloc-vs-ix-vs-loc-explanation-how-are-they-different)
            idx_extremal = dailyData['logRet'].idxmax() if abs(max(dailyData['logRet'])) > abs(min(dailyData['logRet'])) else dailyData['logRet'].idxmin()
            extremalRet = dailyData['logRet'].loc[idx_extremal] 
            extremalTime = dailyData['datetime'].loc[idx_extremal]  # if we just want time: dailyData['datetime'].iloc[idx_extremal].to_pydatetime().time()
           
            # Compute realised variance and bipower variation which is used repeatedly
            BV = bipowerVariation(dailyData['logRet'])
            RV = realisedVariance(dailyData['logRet'])
            
            # Compute daily test statistic (cf. ex., Huang & Tauchen (Eq. 9, 2005) and Dumitru & Urga (Eq, 5, 2012)) 
            testStat = pow(max(1, tripowerVariation(dailyData['logRet']) / (BV * BV)) * c3_GLOBAL / M, - 1.0 / 2.0) * (1 - BV / RV)
       
           # Append output to list
            for idx_m, metric in enumerate([date, RV, BV, testStat, extremalRet, extremalTime]): # should match jumpDataCols 
                jumpTestData[idx_m].append(metric)
        
        # Convert jump test data into pandas dataframe (lists used in loop for speed)     
        jumpTestData = pd.DataFrame(jumpTestData)
        jumpTestData = jumpTestData.transpose()
        jumpTestData.columns = jumpDataCols
        
        # Variable save subfolder
        subFolder = 'BNStest'
        
    # Save results to file (as python variable)
    with open(variableSaveFolder + '\\' + subFolder + '\\' + variableSaveName, 'wb') as f:
        pickle.dump(jumpTestData, f)


''' Functions for analysis of jump test results '''

def uniqueOrderedList(seq, idfun = None): 
    ''' Return unique elements of list whilst preserving order. A optional transfomration function can also be 
    applied to each element in the list. Ref: https://www.peterbe.com/plog/uniqifiers-benchmark.
    Ex.: a=list('ABeeE')  uniqueOrderedList(a, lambda x: x.lower()) ['A','B','e']   '''
    if idfun is None:
        def idfun(x): return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        # in old Python versions:
        # if seen.has_key(marker)
        # but in new ones:
        if marker in seen: continue
        seen[marker] = 1
        result.append(item)
    return result

def sampleMoments(data):
    ''' Return sample moments of dataset as dict '''
    sampleMoments = {fun: getattr(np, fun)(data) for fun in ['mean', 'std']}            # from numpy
    sampleMoments.update({'skew': skew(data), 'excessKurt': kurtosis(data, fisher = True)})   # from scipy package
    sampleMoments.update({'n': len(data)})
    return(sampleMoments)      
        
def analysisLM(data):
    
    print("ADSF")        
    return(sampleMoments(data))
    
class analysisBNS:
    def __init__(self, jumpTestData = None, alpha = None):
        ''' Constructor ''' 
        # Initialise with jump test data if passed in constructor call
        if jumpTestData is not None:
            self.data = jumpTestData    
            
        # False positive rate for BNS test
        if jumpTestData is not None:
            self.alpha = alpha   
    
    def getDetectedJumps(self):      
        # Get returns that reject the null hypothesis of no jump & set as object attribute
        self.detectedJumps = self.data[self.data['testStat'] > norm.ppf(1 - self.alpha)]
    
    def plotDetectedJumps(self, includeNonJumpDays = True, figSize = (15, 6), dotSize = 12, 
                          barWidth = 13, dotColor = 'indianred', barColor = 'cornflowerblue', # https://matplotlib.org/examples/color/named_colors.html
                          fontSize = 12, largeTickLabels = False):
        ''' Two plots for BNS based on jump detections. In each case the y-value is the largest
        daily log return. ''' 
        
        # Get detected jumps if not already done
        if not hasattr(self, 'detectedJumps'): self.getDetectedJumps()
        
        # Get date ranges as datetime objects
        dateRng = {'all': [pd.to_datetime(d) for d in self.data['date']],
                   'jumps': [pd.to_datetime(d) for d in self.detectedJumps['date']]}
        
        # Plot
        if includeNonJumpDays:
            fig, ax = plt.subplots(figsize = figSize) #facecolor = 'w', edgecolor = 'k', dpi = 160
            plt.bar(dateRng['all'], 100.0 * self.data['logRetMax'], width = barWidth, color = barColor)
            ax.scatter(dateRng['jumps'],  100.0 * self.detectedJumps['logRetMax'], s = dotSize, c = dotColor, zorder = 2) # zorder will change which layering order (ex. 1 for scatter below bar, 2 for scatter above bar)
            ax.set_xlim(dateRng['jumps'][0], dateRng['jumps'][-1]) # tighten x-axis
        else:
            fig, ax = plt.subplots(figsize = figSize)
            ax.bar(dateRng['jumps'], 100.0 * self.detectedJumps['logRetMax'], width = barWidth, color = barColor)
            ax.set_xlim(dateRng['jumps'][0], dateRng['jumps'][-1]) # set x-axis dimension to match to entire data set
        #plt.ylabel('Maximum intrarday return (%)', fontsize = fontSize)
        
        # Make ticks larger 
        if largeTickLabels:
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(13)     # yearly ticks
            for tick in ax.yaxis.get_major_ticks():
                 tick.label.set_fontsize(13)    # RV/BV value ticks
          
        # Warning
        # Nb: when time series has too many observations width gets too small and we can't see bar from bar plot, tune 'width' parameter manually to fix this. 
        print("Warning: check each jump detection date has a bar column line to it. If missing, increase 'barWidth' parameter.")
        
        return fig

    
    def plotVariation(self, varType = 'RV', figSize = (15, 6), plotColor = 'cornflowerblue',
                      largeTickLabels = False):
        ''' Plot daily RV or BV (as defined in BNS methods)'''
        # Reformat dates and get RV or BV
        dateRng = [pd.to_datetime(d) for d in self.data['date']]

        # Plot RV in any case (we want RV limits for BV plot), but hide RV plot if BV plot is required
        fig, ax = plt.subplots(figsize = figSize)
        ax.plot(dateRng, self.data.RV, color = plotColor) 
        ax.set_xlim(dateRng[0], dateRng[-1])  # tighten the date limits
        
        # Replot with RV limits if BV plot is required
        if varType is 'BV':
           RVlim = ax.get_ylim()
           fig, ax = plt.subplots(figsize = figSize)
           ax.plot(dateRng, self.data.BV, color = plotColor) 
           ax.set_ylim(*RVlim) 
           ax.set_xlim(dateRng[0], dateRng[-1])  # tighten the date limits
       
        # Make ticks larger 
        if largeTickLabels:
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(13)     # yearly ticks
            for tick in ax.yaxis.get_major_ticks():
                 tick.label.set_fontsize(13)    # RV/BV value ticks
        
        return fig
    
    def plotCondTestStats(self, figSize = (15, 6), barColor = 'cornflowerblue',
                          barWidth = 13, ciColor = 'indianred', fontSize = 12):
        ''' Plot daily daily BNS test statistic if jump is detected (scale axis near 
        CI bound corresponding to alpha of BNS object) '''
        
        # Get detected jumps if not already done
        if not hasattr(self, 'detectedJumps'): self.getDetectedJumps()
        
        # Get date ranges as datetime objects
        dateRng = {'all': [pd.to_datetime(d) for d in self.data['date']],
                   'jumps': [pd.to_datetime(d) for d in self.detectedJumps['date']]}
        
        # Plot
        fig, ax = plt.subplots(figsize = figSize)
        ax.bar(dateRng['jumps'], self.detectedJumps['testStat'], width = barWidth, color = barColor)
        ax.set_xlim(dateRng['all'][0], dateRng['all'][-1]) # set x-axis dimension to match to entire data set
        plt.ylabel('BNS test statistic', fontsize = fontSize)
        
        # Scale y-limits to for nicer plot & mark confidence interval for clarity
        ax.set_ylim(norm.ppf(1 - self.alpha) * 0.9, ax.get_ylim()[1]) 
        ax.axhline(y = norm.ppf(1 - self.alpha), color = ciColor , linestyle = '--' , linewidth = 1.5)
        
        return fig
    
    def plotTestStats(self, alpha = [0.05, 0.01, 0.001], figSize = (15, 6),  
                      barWidth = 13, barColor = 'cornflowerblue', 
                      fontSize = 12,  ciColor = 'indianred',
                      largeTickLabels = False):
        ''' Plot BNS test statistic time series along with confidence interval '''
        # Get confidence interavals
        CI = [norm.ppf(1 - a) for a in alpha]
        
        # Plot
        dateRng = [pd.to_datetime(d) for d in self.data['date']]
        fig, ax = plt.subplots(figsize = figSize)
        ax.bar(dateRng, self.data['testStat'], width = barWidth, color = barColor)
        ax.set_xlim(dateRng[0], dateRng[-1])    # tighten x-axis 
        
        # Make ticks larger 
        if largeTickLabels:
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(13)     # yearly ticks
            for tick in ax.yaxis.get_major_ticks():
                 tick.label.set_fontsize(13)    # test statistic ticks
        
        # Add confidence interval lines
        for ll in CI:
            ax.axhline(y = ll, color = ciColor , linestyle = '--' , linewidth = 2.0)
        return fig

  
    def plotJumpDensity(self, figSize = (10, 6)):
        ''' Plot kernel density of maximum log return on days BNS test detected
        a jump. Note that the density is a function of (1) the return frequency
        and (2) the alpha of the BNS test.'''
        
        # Get detected jumps if not already done
        if not hasattr(self, 'detectedJumps'): self.getDetectedJumps()
        
        # Define function for returning output data for kernel plot
        def kernelPlotData(data, alpha = 0.003):
           ''' Return data pairs for empirical kernel density plot. Domain of 
           plot range determine by empirical quantiles of the data and the 
           desired input alpha level. 
           Dependencies: "from scipy.stats import gaussian_kde, mstats" '''
           kernel = gaussian_kde(data)
           dom = np.linspace(*mstats.mquantiles(data, [alpha, 1 - alpha]), num = 200)
           return(dom, kernel.pdf(dom))
        
        # Plot pdf
        fig, ax = plt.subplots(figsize = figSize)
        ax.plot(*kernelPlotData(list(self.detectedJumps['logRetMax'])))
        return fig

    
    def qqplotTestStats(self, figSize = (10, 6)):
        fig, ax = plt.subplots(figsize = figSize)
        probplot(list(self.data['testStat']), dist = "norm",  plot = plt)
        return fig 
        

