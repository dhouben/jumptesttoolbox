import datetime as dt

class security:
    ''' Security class for high frequency data analysis (including data cleaning) '''

    def __init__(self, name, marketOpen = None, marketClose = None, returnFrequency = None):
        ''' Constructor '''    
        # Security name (string, ex. Reuters identification code / RIC)
        self.name = name
        
        # Time in minutes between each reported security return (ex. 15)
        if returnFrequency is not None:
            if type(returnFrequency) not in [int, float]:
                raise ValueError('Return frequency (in minutes) should be of type int or float')
            else:
                self.returnFrequency = returnFrequency
        
        # Market open and closing times (in LOCAL time)
        for time in [marketOpen, marketClose]: 
            if time is not None:
                if type(time) != dt.time:
                    raise ValueError('Market times should be of type dt.time if specified.')
                else:
                    if time == marketOpen:
                        self.marketOpen = time  
                    else:
                        self.marketClose = time
  
    def getNumberReturnsPerDay(self):
        ''' Function converts open and closing times into number of returns per day '''
        if any([inputArg is None for inputArg in [self.marketOpen, self.marketClose, self.returnFrequency]]):
            # Stop if any requisite input data is missing
           raise ValueError('Market times and return frequency must be specified first.') 
        else:
            # Convert dt.time to dt.datetime to perform arithmetic
            paddedTimes = []
            for time in [self.marketOpen, self.marketClose]:
                paddedTimes.append(dt.datetime(**{'year': 1, 'month': 1, 'day': 1, 'hour': time.hour, 'minute': time.minute, 'second': time.second}))          
            # Number of return periods per day
            temp = divmod((paddedTimes[1] - paddedTimes[0]).total_seconds(), self.returnFrequency * 60)  
            # Return result if length of trading day is a multiple of return frequency
            if temp[1] != 0.0:
                raise ValueError('Length of trading day is not a multiple of reported return frequency.')
            else:
                self.numReturnsPerDay = temp[0]  
    
    def getDataDateRange(self, dataStart, dataEnd):
        ''' Function sets dates for data range of security '''
        if all([type(date) == dt.date for date in [dataStart, dataEnd]]):
            # If start and end dates are of type dt.date add to security object
            self.dataStart = dataStart
            self.dataEnd = dataEnd
        else:
            raise ValueError('Data start and end dates should be of type dt.date.')
    
    def info2(self):
        ''' Print information about security ''' 
        # Print required attributes
        output = "Name: %s" % self.name
        #, self.marketOpen.strftime('%H:%M'), self.marketClose.strftime('%H:%M'),         self.returnFrequency)
        
        # Print optional attributes if present  
        optionalAttr = ['marketOpen', 'marketClose', 'returnFrequency', 'numReturnsPerDay', 'dataStart', 'dataEnd']
        optionalStr = ["\nMarket open:  %s",
                       "\nMarket close: %s",
                       "\nReturn freqency (min):  %d",
                       "\nNumber of returns per day: %d", 
                       "\nData start date: %s" ,
                       "\nData end date: %s"]
        for idx, attribute in enumerate(optionalAttr):
            if hasattr(self, attribute): 
                if attribute in ['dataStart', 'dataEnd']:
                    output += optionalStr[idx] % getattr(self, attribute).strftime('%d %b %Y')
                elif attribute in ['marketOpen', 'marketClose']:
                    output += optionalStr[idx] % getattr(self, attribute).strftime('%H:%M')
                else:
                    output += optionalStr[idx] % getattr(self, attribute)
        print(output)
    
    
    
    
    def info(self):
        ''' Print information about security ''' 
        # Print required attributes
        output = "Name: %s \nMarket open: %s \nMarket close: %s \nReturn frequency: %dmin" % \
            (self.name, self.marketOpen.strftime('%H:%M'), self.marketClose.strftime('%H:%M'), 
             self.returnFrequency)
        # Print optional attributes if present  
        optionalAttr = ['numReturnsPerDay', 'dataStart', 'dataEnd']
        optionalStr = ["\nNumber of returns per day: %d", 
                       "\nData start date: %s" ,
                       "\nData end date: %s" ]
        for idx, attribute in enumerate(optionalAttr):
            if hasattr(self, attribute): 
                if attribute in ['dataStart', 'dataEnd']:
                    output += optionalStr[idx] % getattr(self, attribute).strftime('%d %b %Y')
                else:
                    output += optionalStr[idx] % getattr(self, attribute)
        print(output)