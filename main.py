# -*- coding: utf-8 -*-

''' Module imports '''
import os
import datetime as dt
import pickle
from typing import Dict, List, Any

''' Module imports (third party) '''
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm  # for BNS test statistic (without class implementation) used in 'Table of large jumps'
import statsmodels.api as sm  # for Ljung-Box test stat (in BNS analysis)

''' Module imports (user defined packages) '''
import packages.security as ss
import packages.dataCleaning as dc
import packages.jumpTest as jc

''' Code execution '''


def main():
    ''' Variables relevent for multiple parts of analysis '''
    # Return period (minutes)
    retPeriod = 15

    # Manually initialise all securities (using LOCAL market open and close times from manual inspection of excel files); nb: ordering matters
    securities = [
        ss.security('.FTAS', dt.time(hour=8, minute=0), dt.time(hour=16, minute=30), returnFrequency=retPeriod),
        ss.security('.FTELUK', dt.time(hour=8, minute=0), dt.time(hour=16, minute=30), returnFrequency=retPeriod),
        ss.security('.FTUNUS', dt.time(hour=14, minute=30), dt.time(hour=21, minute=0), returnFrequency=retPeriod),
        ss.security('.SPX', dt.time(hour=8, minute=30), dt.time(hour=15, minute=0), returnFrequency=retPeriod),
        ss.security('.WILREIT', dt.time(hour=9, minute=30), dt.time(hour=16, minute=0), returnFrequency=retPeriod)]

    # Function to retrieve security (by name) from securities list
    def getSecurity(name, securityObjectList):
        for s in securityObjectList:
            if (name or name[1:]) in s.name:
                return s

    # Set & display number of intraday returns for each security
    print('Number of intraday returns:\n')
    for s in securities:
        s.getNumberReturnsPerDay()
        print("%s: %d" % (s.name, s.numReturnsPerDay))
    print('\nUK should be 34, US 26.\n')

    # Set file paths for input and output data (assumes project path is two levels above project directory)
    repoPath = os.getcwd()  # github repository path
    pathSep = '/' if os.name == 'posix' else '\\'  # get os specific path separator ('posix' for unix based  system, 'nt' for windows)
    projectPath = ''.join(t + '/' for t in repoPath.split(sep=pathSep)[:-2])  # overall project path
    filePath = {'in': projectPath + 'Data/uncleanedData/TRTH/' + str(retPeriod) + 'min',
                'out': projectPath + 'Data/cleanedData/TRTH/' + str(
                    retPeriod) + 'min'}  # input and output file directories for data
    filePath = {k: os.path.normpath(path) for k, path in filePath.items()}  # clean windows path for python if necessary

    ''' Data cleaning '''
    cleanData = False
    if cleanData: mainCleaning(securities, retPeriod, filePath)

    ''' Jump test implementation '''
    jumptests = False
    if jumptests:
        # Generate (or retrieve if already in existence) python variables containing jump test results (saved in somePathToWorkingDirectory/variables/someTestName)
        runJumpTest(securities, filePath, 'LM')
        runJumpTest(securities, filePath, 'ABD')
        runJumpTest(securities, filePath, 'BNS')

    ''' Jump test analysis '''
    doAnalysisBNS = True
    if doAnalysisBNS:
        # Load in python variables containing jump test results
        jumpTestData = runJumpTest(securities, filePath, 'BNS')

        ''' Univariate BNS Plots '''
        # For each security in list
        for security in securities:  # [s for s in securities if s.name in ['.FTAS', '.FTELUK']]: #['.SPX', '.FTUNUS']

            # Get data
            data = jumpTestData[security.name]

            # Create BNS object with jump test data and alpha
            BNS = jc.analysisBNS(jumpTestData=data, alpha=0.01)

            # Set generic path for saving images to file (as .png); nb: subfolder based on data frequency
            genericImageSavePath = 'images/%smin/' % retPeriod + 'REPLACE_ME' + security.name[1:] + '.png'

            # Plot daily RV
            tempFig = BNS.plotVariation(varType='RV')  # based on alpha of BNS object
            tempFig.savefig(genericImageSavePath.replace("REPLACE_ME",
                                                         "RV_BNS_open_" + "alpha" + str(BNS.alpha).replace(".",
                                                                                                           "") + "_"),
                            bbox_inches='tight')

            # Plot daily BV (with same y-axis plot limits as RV)
            tempFig = BNS.plotVariation(varType='BV')  # based on alpha of BNS object
            tempFig.savefig(genericImageSavePath.replace("REPLACE_ME",
                                                         "BV_BNS_open_" + "alpha" + str(BNS.alpha).replace(".",
                                                                                                           "") + "_"),
                            bbox_inches='tight')

            # Plot daily BNS test statisic if jump is detected
            tempFig = BNS.plotCondTestStats()  # based on alpha of BNS object
            tempFig.savefig(genericImageSavePath.replace("REPLACE_ME",
                                                         "testStatCond_BNS_open_" + "alpha" + str(BNS.alpha).replace(
                                                             ".", "") + "_"), bbox_inches='tight')

            # Plot maximum returns and highlight when jumps are detected
            tempFig = BNS.plotDetectedJumps(includeNonJumpDays=True)  # based on alpha of BNS object
            tempFig.savefig(genericImageSavePath.replace("REPLACE_ME", "jumpDays_inclRest_BNS_open_" + "alpha" + str(
                BNS.alpha).replace(".", "") + "_"), bbox_inches='tight')

            # Plot maximum returns only on days when jumps are detected
            tempFig = BNS.plotDetectedJumps(includeNonJumpDays=False)  # based on alpha of BNS object
            tempFig.savefig(genericImageSavePath.replace("REPLACE_ME", "jumpDays1_exclRest_BNS_open_" + "alpha" + str(
                BNS.alpha).replace(".", "") + "_"), bbox_inches='tight')

            # QQ plot for BNS test statistic       
            tempFig = BNS.qqplotTestStats()
            tempFig.savefig(genericImageSavePath.replace("REPLACE_ME", "qqPlot_BNS_open_"), bbox_inches='tight')

            # Kernel desnity plot for BNS detected jumps
            tempFig = BNS.plotJumpDensity()
            tempFig.savefig(genericImageSavePath.replace("REPLACE_ME",
                                                         "kernelDensity_BNS_open_" + "alpha" + str(BNS.alpha).replace(
                                                             ".", "") + "_"), bbox_inches='tight')

            # Plot test statistic and CI (1% & 0.1%)
            plottedAlphas = [0.01, 0.001]
            tempFig = BNS.plotTestStats(alpha=plottedAlphas)
            tempFig.savefig(genericImageSavePath.replace("REPLACE_ME",
                                                         "testStat_BNS_open_" + "CIs" + str(plottedAlphas[0]).replace(
                                                             ".", "") + "and" + str(plottedAlphas[1]).replace(".",
                                                                                                              "") + "_"),
                            bbox_inches='tight')

            # Specify if large tick labels plots are also required (needed for small latex plots);
            # functionality is implemented for {plotTestStats, plotVariation, plotDetectedJumps}
            largeTickLabels = True

            # Additional plots with larger tick size labels 
            if largeTickLabels:
                # Change save path for plots with large tick labels
                genericImageSavePath2 = genericImageSavePath.replace('15min', '15min_largeTickLabels')

                # Plot daily RV
                tempFig = BNS.plotVariation(varType='RV', largeTickLabels=True)  # based on alpha of BNS object
                tempFig.savefig(genericImageSavePath2.replace("REPLACE_ME",
                                                              "RV_BNS_open_" + "alpha" + str(BNS.alpha).replace(".",
                                                                                                                "") + "_"),
                                bbox_inches='tight')

                # Plot daily BV (with same y-axis plot limits as RV)
                tempFig = BNS.plotVariation(varType='BV', largeTickLabels=True)  # based on alpha of BNS object
                tempFig.savefig(genericImageSavePath2.replace("REPLACE_ME",
                                                              "BV_BNS_open_" + "alpha" + str(BNS.alpha).replace(".",
                                                                                                                "") + "_"),
                                bbox_inches='tight')

                # Plot maximum returns only on days when jumps are detected
                tempFig = BNS.plotDetectedJumps(includeNonJumpDays=False,
                                                largeTickLabels=True)  # based on alpha of BNS object
                tempFig.savefig(genericImageSavePath2.replace("REPLACE_ME",
                                                              "jumpDays1_exclRest_BNS_open_" + "alpha" + str(
                                                                  BNS.alpha).replace(".", "") + "_"),
                                bbox_inches='tight')

                # Plot test statistic and CI (1% & 0.1%)
                plottedAlphas = [0.01, 0.001]
                tempFig = BNS.plotTestStats(alpha=plottedAlphas, largeTickLabels=True)
                tempFig.savefig(genericImageSavePath2.replace("REPLACE_ME", "testStat_BNS_open_" + "CIs" + str(
                    plottedAlphas[0]).replace(".", "") + "and" + str(plottedAlphas[1]).replace(".", "") + "_"),
                                bbox_inches='tight')

        ''' Table of jump proportions '''
        # Initialise significance level & data frame for results
        alphas = [0.05, 0.01, 0.005, 0.001]
        jumpProp = pd.DataFrame(columns=['Security', 'Sample', 'N'] + [str(a) for a in alphas])
        for idx_s, security in enumerate(securities):
            # Get data
            data = jumpTestData[security.name]

            # Get date range for security
            dateRngTemp = [data['date'].iloc[ii].strftime("%d/%m/%y") for ii in [0, -1]]
            dateRngTemp = '-'.join(dateRngTemp)

            # Get number of days
            numDays = data.shape[0]

            # Create BNS object with jump test data and alpha
            BNS = jc.analysisBNS(jumpTestData=data, alpha=0.01)

            # Compute proportions each alpha level
            proportions = []
            for alpha in alphas:
                # Reset alpha and get detected jumps given this alpha
                BNS.alpha = alpha
                BNS.getDetectedJumps()

                # Get proportion of days with jump detected
                proportions.append(BNS.detectedJumps.shape[0] / BNS.data.shape[0])

            # Convert proportions to strings with desired number of decimal places
            proportions = ['%.5f' % p for p in proportions]

            # Save to dataframe
            jumpProp.loc[idx_s] = [security.name, dateRngTemp, str(numDays)] + proportions

        # Push table to latex & write to file
        caption = '\caption{Proportion of days the BNS test rejected the null hypothesis of no jumps using %.0f minute returns.} \n' % retPeriod
        label = '\label{tab:BNSjumpProportions_%.0fm} \n' % retPeriod
        latexTable = jumpProp.to_latex(index=False)
        latexTable = latexTable + caption + label
        with open("latexTables/jumpProportions_BNS.txt", "w") as f:
            f.write(latexTable)

        ''' Table of jump descriptive statistics [sign conditional jumps and multiple alphas]'''
        # Initialise significance level & data frame for results
        numStats = 6  # {numJumps, proportion of jumps, mean, std, skew, kurt}
        alphas = [0.05, 0.01, 0.001]
        jumpCond = pd.DataFrame(columns=['Security', 'Sample'] + ['J+', 'J-', 'J'] * len(alphas),
                                index=range(len(securities) * numStats))
        for idx_s, security in enumerate(securities):
            # Get data
            data = jumpTestData[security.name]

            # Create BNS object with jump test data and alpha
            BNS = jc.analysisBNS(jumpTestData=data, alpha=0.01)

            # Get statistics for each alpha level
            for idx_a, alpha in enumerate(alphas):
                # Reset alpha and get detected jumps given this alpha
                BNS.alpha = alpha
                BNS.getDetectedJumps()

                # Condition on positive and negative jumps
                Jall = BNS.detectedJumps
                Jplus = Jall[Jall['logRetMax'] >= 0]['logRetMax']
                Jminus = Jall[Jall['logRetMax'] < 0]['logRetMax']

                # Get number and proportion of days with jump detected
                Jagg = [Jplus, Jminus, Jall['logRetMax']]
                # Jcount = [getattr(JJ, 'shape')[0] for JJ in Jagg]
                # Jprop = ['%.5f' % (JJ / BNS.data.shape[0]) for JJ in Jcount]

                # Get sample moments
                d = [jc.sampleMoments(JJ) for JJ in Jagg]

                for idx_d, dd in enumerate(d):
                    # Add count and proportion values to dictionary of sample stats
                    dd['prop'] = dd['n'] / BNS.data.shape[0]

                    # Rename excess kurtosis and standard deviation  
                    formattedKeys = [key.replace('excessKurt', 'kurt').replace('std', 'std dev') for key in d[0].keys()]

                    # Get row range for relevant security (for dataframe population)
                    rowRefs = range((idx_s * len(dd)),
                                    (idx_s * len(dd) + len(dd)))  # redundancy, but in nested loop b/c depends on d

                    # Add descriptive statistics values to dataframe
                    # formattedVals = ['%.4f' % ddd for ddd in dd.values()]
                    formattedVals = ['%.4f' % list(dd.values())[0],
                                     '%.4f' % list(dd.values())[1],
                                     '%.3f' % list(dd.values())[2],
                                     '%.3f' % list(dd.values())[3],
                                     '%.0f' % list(dd.values())[4],
                                     '%.4f' % list(dd.values())[5]]
                    jumpCond.iloc[rowRefs, 2 + idx_a * 3 + idx_d] = formattedVals

                    # Add security name (with padding)
                    jumpCond.iloc[rowRefs, 0] = [security.name] + ['' for i in range(len(formattedKeys) - 1)]

                    # Add descriptive statistic names
                formattedKeys = [key.replace('excessKurt', 'kurt').replace('std', 'std dev') for key in
                                 d[0].keys()]  # rename excess kurtosis and standard deviation
                jumpCond.iloc[rowRefs, 1] = formattedKeys

        # Push table to latex
        caption = '\caption{Descriptive statistics for the largest intraday return on days when a jump has been detected by the BNS test using %.0f minute returns. Statistics for all detected jumps are denoted by $J$, while $J\pm$ denotes the set of jumps where the largest intraday return on a jump detection day was positive and negative, respectively. The statistics $n$, prop, and kurt denote the number of detected jumps, the number of detected jumps as a proportion of the number of days in the sample, and excess kurtosis, respectively. ADD REFERENCE TO SAMPLE PERIODS, MATHFONT FOR J, CENTRE COLUMN NAMES} \n' % retPeriod
        label = '\label{tab:BNSdescriptiveStats_MaxReturns_%.0fm} \n' % retPeriod
        latexTable = jumpCond.to_latex(index=False)
        latexTable = latexTable + caption + label

        # Insert line of latex code for alphas in latex table 
        alphaLine = '\multicolumn{2}{c}{Significance Level}' + ''.join(
            [' & \multicolumn{3}{c}{%.3f}' % aa for aa in alphas]) + '\\\ \cline{%.0f-%0.f}' % (
                        len(alphas), len(alphas) * 3 + 2) + ' \n '
        idx_start = latexTable.index('Security')
        latexTable = latexTable[0:idx_start] + alphaLine + latexTable[idx_start:]

        # Write to file
        with open("latexTables/jumpStatDescription_maxReturns_BNS.txt", "w") as f:
            f.write(latexTable)

        ''' Table of jump descriptive statistics'''
        numStats = 6  # mean, std, skew, excess kurtosis
        jumpDesc = pd.DataFrame(columns=['Security', 'Sample', 'RV', 'BV', 'RV -- BV', 'RJ', 'Test stat'],
                                index=range(len(securities) * numStats))
        for idx_s, security in enumerate(securities):
            # Get data
            data = jumpTestData[security.name]

            # Compute mean and std deviation of BNS statistics
            stats = [data['RV'],
                     data['BV'],
                     data['RV'] - data['BV'],
                     (data['RV'] - data['BV']) / data['RV'],
                     data['testStat']]

            # Write sample statistics to dataframe
            for idx_t, statistic in enumerate(stats):
                # Get descriptive statistics 
                d = jc.sampleMoments(statistic)
                d.pop('n')  # remove unwanted statistics

                # Compute Ljung-Box test statistic (and p-value) at maxLag
                maxLag = 15
                lbVals = sm.stats.acorr_ljungbox(list(statistic), lags=maxLag)
                lbStat, lbPval = [lbVals[ii][-1] for ii in range(2)]

                # Add LB values to dictionary of sample stats
                d.update({'LB_%i' % maxLag: lbStat})
                d.update({'p-val': lbPval})

                # Check dimensions
                if numStats != len(d): raise ValueError("numStats must equal number of descriptive statistics")

                # Get row range for relevant security (for dataframe population)
                rowRefs = range((idx_s * len(d)),
                                (idx_s * len(d) + len(d)))  # redundancy, but in nested loop b/c depends on d

                # Add descriptive statistic names
                formattedKeys = [key.replace('excessKurt', 'kurt').replace('std', 'std dev') for key in
                                 d.keys()]  # rename excess kurtosis and standard deviation
                jumpDesc.iloc[rowRefs, 1] = formattedKeys  # redundancy, but in nested loop b/c depends on d

                # Add descriptive statistics values to dataframe
                formattedVals = ['%.5f' % dd for dd in d.values()]
                jumpDesc.iloc[rowRefs, idx_t + 2] = formattedVals

                # Add security name (with padding)
                jumpDesc.iloc[rowRefs, 0] = [security.name] + ['' for i in range(len(d) - 1)]

        # Push table to latex & write to file
        caption = '\caption{Descriptive statistics for daily BNS statistics using %.0f minute returns. RJ denotes the proportion of the daily realised variance that is attributable to jumps, that is RJ $\\equiv$ (RV -- BV)/RV. Excess kurtosis is reported. CHANGE TEST STAT to ZpowBNS, ADD REFERENCE TO SAMPLE PERIODS..., CENTRE COLUMN NAMES, REDUCE D.P. FOR SKEW AND KURT} \n' % retPeriod
        label = '\label{tab:BNSdescriptiveStats_%.0fm} \n' % retPeriod
        latexTable = jumpDesc.to_latex(index=False)
        latexTable = latexTable + caption + label
        with open("latexTables/jumpStatDescription_BNS.txt", "w") as f:
            f.write(latexTable)

        ''' Table of large jumps '''
        # For each security in the pair being analysed, get dates on which extreme inferred jump returns occurred. Then
        # compute various metrics for all extreme days for both securities and output in a dataframe which is then
        # converted to a latex table. Note that extremal_dates (defined below) provides a (possibly many-to-many) map
        # from the security to the extremal date.

        # Specify user inputs
        num_movements = 5  # number of extreme days to consider
        table_alpha = 0.01  # alpha for BNS test
        test_significance = True  # True if stars are output in table when BNS test statistic is significant at alpha
        latex_table_landscape = True  # True for landscape latex table orientation, false for portrait
        pairs = [['.FTAS', '.FTELUK'],          # UK equity-reit
                 ['.SPX', '.FTUNUS'],           # US equity-reit
                 ['.FTELUK', '.FTUNUS'],        # Reit UK-US
                 ['.FTAS', '.SPX']]             # Equity UK-US

        # Do analysis for each specified pair
        for pair in pairs:

            # Preallocate dictionaries for: (i) map from security to extreme dates, and (ii) extreme dates with no security
            # map. Note that extremal_dates (defined below) provides a (possibly many-to-many) map from the security to the
            # extremal date.
            extremal_dates = {sign: {name: [] for name in pair} for sign in ['positive', 'negative']}
            extremal_dates_flat = {sign: [] for sign in ['positive', 'negative']}

            # For each security
            for name in pair:
                # Get security data
                df = jumpTestData[name]

                # Sort on extreme returns (then by date)
                df_sorted = df.sort_values(by=['logRetMax', 'date'], ascending=[False, True])

                # Obtain dates when each security had the most extreme inferred jump returns
                extremal_dates['positive'][name] = df_sorted['date'].head(n=num_movements).tolist()
                extremal_dates['negative'][name] = df_sorted['date'].tail(n=num_movements).tolist()

                # Flatten the dates dictionary (to remove security dependence)
                extremal_dates_flat['positive'].append(extremal_dates['positive'][name])
                extremal_dates_flat['negative'].append(extremal_dates['negative'][name])

            # Flatten date sublists (as well as sort & remove any duplicate dates)
            flatten = lambda l: [item for sublist in l for item in sublist]
            for sign in ['positive', 'negative']:
                extremal_dates_flat[sign] = sorted(set(flatten(extremal_dates_flat[sign])))

            # Get number of unique dates
            num_unique_pos, num_unique_neg = [len(l) for l in extremal_dates_flat.values()]

            # Initialise table
            num_table_vars = 5  # {date, local time, maxLogRet, (RV-BV)/RV * 100, in top num_movements)
            jump_events = pd.DataFrame(columns=['Date'] + [pp[1:] for pp in pair] * num_table_vars,
                                       index=range(num_unique_pos + num_unique_neg))

            # Function to splice two lists together into one
            def splice(list_one, list_two):
                return flatten([[aa, bb] for aa, bb in zip(list_one, list_two)])

            # Local function for BNS significance test
            def local_bns_test(test_stat, alpha):
                return test_stat > norm.ppf(1 - alpha)

            # Populate table (each row is a unique date)
            for idx_date, date in enumerate(flatten(extremal_dates_flat.values())):
                metrics = {p: [] for p in pair}
                for name in pair:
                    # Get data
                    df = jumpTestData[name]
                    temp = df[df['date'] == date]

                    # Get metrics
                    metrics[name] = [temp['timeMax'].item().time(),  # local time
                                     temp['logRetMax'].item(),  # inferred jump return
                                     1 - temp['BV'].item() / temp['RV'].item(),  # proportion of RV due to jump
                                     temp['testStat'].item(),  # BNS test statistic
                                     date in extremal_dates['positive'][name]  # extremal date for current security?
                                     + extremal_dates['negative'][name]]

                    # Formatting functions for metrics
                    fmt__fns = [lambda x: x.strftime('%H:%M'),
                                lambda x: '%.3f' % x,
                                lambda x: '%.3f' % x,
                                lambda x: '%.2f' % x,
                                lambda x: '%s' % x]

                    # Test for significance of BNS test statistic (for starred output in table)
                    if test_significance:
                        star = "*" if local_bns_test(temp['testStat'].item(),
                                                     table_alpha) else " "  # star string if significant, otherwise empty string
                        fmt__fns[-2] = lambda x: '%.2f%s' % (x, star)  # append start to BNS test statistic if significant

                    # Format metrics for output
                    metrics[name] = list(map(lambda x, y: x(y), fmt__fns, metrics[name]))

                # Push data into table
                jump_events.iloc[idx_date, :] = [date] + splice(*list(metrics.values()))

            # Dictionary (can be recycled for other tables)
            number_word_dict = {ii: number for ii, number in enumerate(['zero', 'one', 'two', 'three', 'four', 'five',
                                                                        'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
                                                                        'twelve', 'thirteen', 'fourteen', 'fifteen'])}
            # Latex caption and label
            caption = ('The %s largest positive and negative inferred jump returns, from \eqref{eq:dailyJump_maxRet}, for '
                       'both the %s and %s indices using %d minute returns over the sample period '
                       '%s through %s. ') % (number_word_dict[num_movements], pair[0][1:], pair[1][1:], retPeriod,
                                            *get_return_range(security_name=name, jump_test_data=jumpTestData,
                                                                   output_str=True, str_fmt='%d/%m/%y'))  # or *[p[1:] for p in pair]
            caption += ('Dates in the upper and lower parts of the table relate to positive and negative jump returns, respectively. '
                    'The Top %d variable denotes whether or not the return on a given date was one of the %s largest '
                    'intraday returns (of a given sign) for the relevant security. ') % (num_movements, number_word_dict[num_movements])
            if test_significance:
                caption += 'Stars indicate significance of the BNS test at the %.0f\%% level.' % (100 * table_alpha)
            caption = '\caption{' + caption + '} \n'
            label = 'largeJumps_BNS_%s_%s_%.0fm' % (*[p[1:] for p in pair], retPeriod)
            if test_significance:
                label += '_alpha' + str(table_alpha).replace(".", "")
            latex_label = '\label{tab:' + label + '} \n'

            # Custom header for latex table
            latex_header = ('& \multicolumn{2}{c}{Local Time} '
                            '& \multicolumn{2}{c}{Jump Return} '
                            '& \multicolumn{2}{c}{$(\\text{RV} - \\text{BV}) / \\text{RV}$} '
                            '& \multicolumn{2}{c}{BNS Test Stat} '
                            '& \multicolumn{2}{c}{Top %d} '
                            '\\\ \cline{2-11} ') % num_movements
            latex_header += 'Date ' + ('& %s & %s ' % (pair[0][1:], pair[1][1:]) * num_table_vars) + '\\\ '

            # Write latex table to file
            latex_table = jump_events.to_latex(index=False, header=False)
            latex_table = latex_table + caption + latex_label

            # Insert midrule to separate postive from negative jumps in latex table
            # See, ex.: https://stackoverflow.com/questions/32275070/midrule-in-latex-output-of-python-pandas#32276740
            if True:
                latex_list = latex_table.splitlines()
                latex_list.insert(num_unique_pos + 2, '\\midrule') # +2 for \begin{tabular} and \toprule
                latex_table = '\n'.join(latex_list)

            # Insert custom latex_header after \toprule
            identifier_header = '\\toprule'
            idx_start = latex_table.index(identifier_header) + len(identifier_header)
            latex_table = latex_table[0:idx_start] + '\n ' + latex_header + ' \n \\midrule' + latex_table[idx_start:]
            latex_table = '\\begin{table} \n\\centering \n' + latex_table + '\\end{table} \n'
            if latex_table_landscape:
                latex_table = '\\begin{landscape}\n' + latex_table + '\\end{landscape}\n'

            # Write latex table to file
            with open('latexTables/' + label + '.tex', "w") as f:
                f.write(latex_table)









        with open('/home/dirk/Desktop/testLatex.txt', 'w') as out:
            out.write(latexTable + '\n')


        # do we set header=False (and manually write our latex header into pyhton code here)?

        ### "Comment on asymmetry in tail dependence: 1. most signs tend to be the same for negative (not so for positive
        # 2. more instances when both assets are in top 5 (i.e. duplicate dates) # " ###




        ''' BNS cojump analysis '''

        # 1. Identify days of cojumps
        # 2. Correlation of BNS test stats over full sample
        # 3.

        BNS.getDetectedJumps()
        BNS.detectedJumps[BNS.detectedJumps['logRetMax'] == BNS.detectedJumps['logRetMax'].min()]
        BNS.data[BNS.data['logRetMax'] == BNS.data['logRetMax'].min()]

        import packages.jumpTest as jc
        BNS = jc.analysisBNS(jumpTestData=data, alpha=0.01)

        BNS.data.head()
        plt.subplots(figsize=(15, 9))
        plt.plot(range(BNS.data.shape[0]), BNS.data['RV'] - BNS.data['BV'])

        # Day when BV was largest in magnitude relative to RV
        BNS.data[BNS.data['RV'] - BNS.data['BV'] == min(BNS.data['RV'] - BNS.data['BV'])]

        # Get days where BNS test detects jump (according to pre-specified alpha)
        BNS.getDetectedJumps()


''' Wrapper for each part of analysis (these are called in main()) '''


def runJumpTest(securities, filePath, testType):
    ''' Run (or retrieve, if already run) jump tests (based on user created jumpTest package) '''

    # Subfolder for specific test type results (cf. 'variableSaveFolder' below)
    testSubfolder = {'LM': 'LMtest', 'ABD': 'ABDtest', 'BNS': 'BNStest'}

    # Ensure testType is valid
    if testType not in list(testSubfolder.keys()): raise ValueError("testType must be one of {'LM', 'ABD', 'BNS'}")

    # Set parameters for Lee & Mykland (2008) jump test (None values reset later, since security dependent) 
    # Nb: ABD and BNS  do not require any additional parameters at this point
    LMparams = {'windowSize': None,
                # (security dependent) window size based on recommendation in Lee & Mykland (p. 2542, 2008)
                'alpha': 0.01,  # alpha for hypothesis testing
                'n': None}  # number of test statistics that can be generated

    # Get file names of cleaned data
    fileNameClean = [f for f in os.listdir(filePath['out']) if f.endswith('.csv')]
    fileNameClean = {'intraday': [f for f in fileNameClean if 'intraday' in f],
                     'overnight': [f for f in fileNameClean if 'overnight' in f]}

    # Subpath in current working directory for saving jump test data (as python variable)
    pathSep = '/' if os.name == 'posix' else '\\'  # get os specific path separator ('posix' for unix based  system, 'nt' for windows)
    variableSaveFolder = os.getcwd() + pathSep + 'variables'
    if not os.path.isdir(variableSaveFolder): raise ValueError(
        "'variables' subfolder should exist in working directory")

    # Get names for jump test result output (identified by return frequency, security & data dates; 1:1 map with cleaned data files)
    variableNames = [os.path.splitext(n)[0] for n in fileNameClean['intraday']]

    # Reorder variableNames according to list of securities objects
    def myInefficientListSort(listUnorderd, listOrdered):
        ''' The unordered list contains strings that we want to order according to the substrings in the ordered list '''
        idx_sort = []
        for substr in listOrdered:
            for idx_supstr, supstr in enumerate(variableNames):
                if substr in supstr: idx_sort.append(idx_supstr)
        out = []
        for idx in idx_sort:
            out.append(listUnorderd[idx])
        return (out)

    variableNames = myInefficientListSort(variableNames, [s.name[1:] for s in securities])

    # Create dictionary indexed by security names for saving jump test data
    jumpTestData = {k: None for k in [s.name for s in securities]}

    # Test if jump test results already exists & load them in if so
    for name, s in zip(variableNames, securities):

        # Get security name
        securityName = s.name

        # Raise error if there is not a corresponding variable name for each security
        if securityName[1:] not in name: raise ValueError("variableNames and securities not ordered correctly")

        # Switch for exit if the test results already exist
        jumpDataLoadDone = False

        # Load or create jump test data
        while not jumpDataLoadDone:
            try:
                # Try to load in python variables with jump test results
                with open(variableSaveFolder + pathSep + testSubfolder[testType] + pathSep + name, 'rb') as f:
                    jumpTestData[securityName] = pickle.load(f)
                print("Jump test data exists, now loaded in from file (to jumpTestData dict).")
                jumpDataLoadDone = True
            except FileNotFoundError:
                # If jump test results dont exist run jump tests & save output to file
                print("Jump test data does not exist, calling jump test implementation functions.")
                if testType == "LM":
                    jc.jumpTest(s, testType, LMparams, fileNameClean, filePath, variableSaveFolder, name)
                elif testType == "ABD":
                    jc.jumpTest(s, testType, {}, fileNameClean, filePath, variableSaveFolder, name)
                elif testType == "BNS":
                    jc.jumpTest(s, testType, {}, fileNameClean, filePath, variableSaveFolder, name)

    return (jumpTestData)


def mainCleaning(securities, retPeriod, filePath):
    ''' Script for cleaning high frequency data, based on user created dataCleaning package.
    Nb: works for 'Open' price data (not just 'Last'), but output is named as 'last' to 
    to avoid having to change existing code. '''

    # Filenames for output / cleaned data (these are adjusted below based on security)
    fileNameOut = {'intraday': 'logRet_intraday', 'overnight': 'logRet_overnight'}

    # Get names of input csv files
    fileNameIn = [f for f in os.listdir(filePath['in']) if f.endswith('.csv')]

    # Get security names (RIC codes)
    securityNames = ["." + f[:-8] for f in fileNameIn]

    # Check all existing securities have been initialised
    if [s.name for s in securities] != securityNames:
        raise ValueError(
            'Some secuirities not initialised or securities list is not ordered according to securityNames.')

    # Data cleaning parameters
    cleaningParams = {'missingReturnThreshold': 4,  # missing returns permitted on any given day 
                      'zeroReturnThreshold': 3,  # number of exactly zero returns permitted on any given day
                      'doDiagnostics': True}  # save information about missing data if true

    # Clean data & write results to file
    dc.cleanData(securities, cleaningParams, fileNameIn, fileNameOut, filePath)


def get_return_range(security_name, jump_test_data, output_str=True, str_fmt='%d/%m/%y'):
    ''' Output security data range given security name and the jump_test_data dictionary (which contains
     a dataframe for each security. '''
    out = [jump_test_data[security_name]['date'].tolist()[idx] for idx in [0, -1]]
    if output_str:
        out = [o.strftime(str_fmt) for o in out]
    return out


''' Run main() '''
if __name__ == '__main__':
    main()
